'use strict'

const _ = require('lodash')

class KdniaoError {
  /**
   * Static Method for getting REQUEST error
   *
   * @param error_enum {object}
   * @param error_data {object} - The object trigger the error
   * @param retry_count {number}
   * @constructor
   */
  static getRequestError (error, errorData, retryCount) {
    error.data = errorData
    error.type = error.code

    if (retryCount) {
      error.retry_count = retryCount
    }

    return error
  }

  /**
   * Static Method for getting SDK error
   *
   * @param error_enum {object}
   * @param error_data {object} - The object trigger the error
   * @param retry_count {number}
   * @constructor
   */
  static getSdkError (errorEnum, errorData) {
    let error = new Error()
    error.type = errorEnum.type
    error.message = errorEnum.message
    error.data = errorData
    Error.captureStackTrace(error)

    return error
  }

  /**
   * Static Method for getting API error
   *
   * @param response_body {object}
   * @param retry_count {number}
   * @constructor
   */
  static getApiError (responseBody, retryCount) {
    let error = new Error()
    error.type = _.get(responseBody, 'meta.type')
    error.message = _.get(responseBody, 'meta.message')
    error.code = _.get(responseBody, 'meta.code')
    error.data = _.get(responseBody, 'data')
    error.response_body = JSON.stringify(responseBody)

    if (retryCount) {
      error.retry_count = retryCount
    }

    return error
  }
}

module.exports = KdniaoError
