'use strict'

// Declare imports
const _ = require('lodash')
const Error = require('./error/error')
const ErrorEnum = require('./error/error_enum')

// Global variable
const RetryLimit = 5
// const UnauthorizedError = 401
// const TooManyRequestError = 429
const RetriableApiError = [500, 502, 503, 504]
const RetriableRequestError = ['ETIMEDOUT', 'ECONNRESET', 'ECONNREFUSED']

// Handler class
class Handler {
  /**
   * Function to get the payload and create the request
   *
   * @param kdniao {kdniao} - the kdniao object
   * @param payload {object} - The payload object to create the request
   * @param callback {function}
   */
  static handlePayload (kdniao, payload, callback) {
    // console.log(payload.request_object)
    kdniao.request(payload.request_object, function (err, response, body) {
      if (err) {
        // If request return err
        if (RetriableRequestError.indexOf(err.code) !== -1) {
          // Retry if err is retriable
          Handler._retryRequestError(kdniao, payload, err, callback)
        } else {
          // Return err if not retriable
          callback(err)
        }
      } else {
        if (_.isString(body)) {
          // Non json response, throw error
          let internalError = {
            meta: _.cloneDeep(ErrorEnum.InternalError)
          }
          internalError.meta.code = 500
          internalError.data = payload.request_object.body || {}
          Handler._retryApiError(kdniao, payload, internalError, callback)
        } else {
          if (RetriableApiError.indexOf(response.statusCode) !== -1) {
            // Retry if err is RetriableApiError
            Handler._retryApiError(kdniao, payload, body, callback)
          } else if (response.statusCode !== 200 && response.statusCode !== 201) {
            // Return err if it is not OK response
            callback(Error.getApiError(body))
          } else {
            // Response OK
            if (payload.raw) {
              // If raw is true, response string
              callback(null, JSON.stringify(body))
            } else {
              // Else response Object
              callback(null, body)
            }
          }
        }
      }
    })
  }

  /**
   * Function to retry request error
   *
   * @param kdniao {kdniao}
   * @param payload {object} - The payload object to create the request
   * @param err {error} - the error object request return
   * @param callback {function}
   */
  static _retryRequestError (kdniao, payload, err, callback) {
    // If retry is true && retry_count < 5
    if (payload.retry && payload.retry_count < RetryLimit) {
      // Increase retry_count
      payload.retry_count++
      // Retry after 1s
      setTimeout(function () {
        Handler.handlePayload(kdniao, payload, callback)
      }, 1000)
    } else {
      // Return err
      callback(Error.getRequestError(err, payload.request_object, payload.retry_count))
    }
  }

  /**
   * Function to retry API error (code >= 500)
   *
   * @param kdniao {kdniao}
   * @param payload {object} - The payload object to create the request
   * @param body {object} - If it is an RetriableError, body is the response body, else is the request error object
   * @param callback {function}
   */
  static _retryApiError (kdniao, payload, body, callback) {
    // If retry is true && retry_count < 5
    if (payload.retry && payload.retry_count < RetryLimit) {
      // Increase retry_count
      payload.retry_count++
      // Retry after 1s
      setTimeout(function () {
        Handler.handlePayload(kdniao, payload, callback)
      }, 1000)
    } else {
      // Return err
      callback(Error.getApiError(body, payload.retry_count))
    }
  }
}

module.exports = Handler
