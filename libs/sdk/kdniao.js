'use strict'

const _ = require('lodash')
const request = require('request')
const Handler = require('./handler')
const Payload = require('./payload')
const Error = require('./error/error')
const ErrorEnum = require('./error/error_enum')

const DefaultEndpoint = process.env.KDNIAO_API_ENDPOINT
const Methods = [ 'GET', 'POST' ]

class Kdniao {
  constructor (apiKey, options) {
    this._errorHandling(apiKey, options)

    // Setup
    options = options || {}
    this.request = request
    this.apiKey = apiKey
    this.endpoint = options.endpoint || DefaultEndpoint
    this.retry = _.isBoolean(options.retry) ? options.retry : true

    // Create proxy methods
    for (let i = 0; i < Methods.length; i++) {
      let _this = this
      this[Methods[i]] = function () {
        let args = Array.prototype.slice.call(arguments)
        args.unshift(Methods[i])
        _this.call.apply(_this, args)
      }
    }
  }

  /**
   * Call (Context-less)
   *
   * @param method {string} - get, post, put or delete
   * @param path {string} - pathname for URL
   * @param options {object}
   *   body {object} - POST body
   *   query {object} - query object
   *   retry {boolean} - Retry if fail? override this.retry if set
   *   raw {boolean} - if true, return string, else return object, default false
   * @param callback {function}
   */
  call (method, path, options, callback) {
    let _this = this
    // retrieve arguments as array
    let args = []
    for (let i = 0; i < arguments.length; i++) {
      args.push(arguments[i])
    }

    // if last element is a callback
    // store it to callback
    if (typeof args[args.length - 1] === 'function') {
      callback = args.pop()
    } else {
      callback = null
    }

    // Create payload with (kdniao, method, path, options)
    let payload = Payload(_this, args[0], args[1], args[2])

    if (callback) {
      // Handle the payload, with the callback
      Handler.handlePayload(_this, payload, callback)
    } else {
      // return Promise, is callback is not define
      return new Promise(function (resolve, reject) {
        Handler.handlePayload(_this, payload, function (err, result) {
          if (err) {
            reject(err)
          } else {
            resolve(result)
          }
        })
      })
    }
  }

  /**
  * Error Handling function
  * Throw error if the input param contain incorrect type
  */
  _errorHandling (apiKey, options) {
    if (!_.isString(apiKey)) {
      // Verify api_key
      throw Error.getSdkError(ErrorEnum.ConstructorInvalidApiKey, apiKey)
    } else if (!_.isNull(options) && !_.isUndefined(options) && !_.isPlainObject(options)) {
      // Verify options
      throw Error.getSdkError(ErrorEnum.ConstructorInvalidOptions, options)
    }

    // Verify options value
    if (options) {
      if (!_.isNull(options.endpoint) && !_.isUndefined(options.endpoint) && !_.isString(options.endpoint)) {
        // Verify endpoint
        throw Error.getSdkError(ErrorEnum.ConstructorInvalidEndpoint, options.endpoint)
      } else if (!_.isNull(options.retry) && !_.isUndefined(options.retry) && !_.isBoolean(options.retry)) {
        // Verify retry
        throw Error.getSdkError(ErrorEnum.ConstructorInvalidRetry, options.retry)
      }
    }
  }
}

// Exports the constructor
module.exports = function (apiKey, options) {
  return new Kdniao(apiKey, options)
}
