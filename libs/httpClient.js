'use strict'

const md5 = require('md5')
const b64 = require('js-base64').Base64

class httpClient {
  static encryptBody (requestData, requestType) {
    const body = {
      RequestData: JSON.stringify(requestData),
      EBusinessID: process.env.KDNIAO_USER_ID,
      RequestType: requestType,
      DataSign: b64.encode(md5(JSON.stringify(requestData) + process.env.KDNIAO_API_KEY)),
      DataType: 2
    }

    return body
  }
}

module.exports = httpClient
