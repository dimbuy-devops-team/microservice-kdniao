'use strict'

/**
 * Error enum class
 */
const RequestEnum = {
  Track: {
    type: 1002,
    path: '/api-track',
    description: '物流查询API提供实时查询物流轨迹的服务，用户提供运单号和快递公司，即可查询当前时刻的最新物流轨迹。'
  },
  Recognise: {
    type: 2002,
    path: '/api-recognise',
    description: '单号识别API为用户提供单号识别快递公司服务，依托于快递鸟大数据平台，用户提供快递单号，即可实时返回可能的一个或多个快递公司，存在多个快递公司结果的，大数据平台根据可能性、单号量，进行智能排序。'
  }

}

module.exports = RequestEnum
