'use strict'

const Waybills = require('../handlers/waybills')
const Favicons = require('../handlers/favicons')

const routes = [
  { method: 'GET', options: Favicons.index, path: '/favicon.ico' },
  { method: 'GET', options: Waybills.recognise, path: '/waybills/{waybill_no}' },
  { method: 'GET', options: Waybills.track, path: '/waybills/{waybill_no}/companys/{shipper_code}' }
]

exports.routes = server => server.route(routes)
