'use strict'

const chinese = require('chinese-s2t')
const boom = require('@hapi/boom')
const httpClient = require('../../libs/httpClient')
const waybillSchemas = require('../schemas/waybills')
const RequestEnum = require('../../libs/requestEnum')
const Kdniao = require('../../libs/sdk/kdniao')(process.env.KDNIAO_API_KEY, { retry: false })

/*
 * 單號識別API 返回 JSON
 * {
 *   "EBusinessID": "1257021",
 *   "Success": true,
 *   "LogisticCode": "3967950525457",
 *   "Shippers": [{
 *     "ShipperCode": "YD",
 *     "ShipperName": "韻達快遞"
 *   }]
 * }
 */

exports.recognise = {
  validate: {
    params: waybillSchemas.queryParams
  },
  handler: async (request, h) => {
    try {
      const waybillNo = request.params.waybill_no.replace(/\s+/g, '').trim()

      const requestData = {
        LogisticCode: waybillNo
      }

      const responseData = await Kdniao.call('POST', RequestEnum.Recognise.path, {
        body: httpClient.encryptBody(requestData, RequestEnum.Recognise.type)
      }).then(result => {
        return result
      }).catch(err => {
        throw boom.badRequest(`系統錯誤: ${err.response_body}`)
      })

      const output = {
        success: responseData.Success,
        waybill_no: responseData.LogisticCode,
        shippers: responseData.Shippers.map((shipper, idx, ary) => {
          return {
            code: shipper.ShipperCode,
            name: chinese.s2t(shipper.ShipperName)
          }
        })
      }

      // let { EBusinessID, ...o2 } = resultBody

      return h.response(output).code(200)
    } catch (e) {
      return h.badRequest(`系統錯誤: ${e.message}`)
    }
  }
}

/*
 *
 * 即時查詢API 返回 JSON
 * ### 物流狀態：2-在途中,3-簽收,4-問題件
 *
 * 沒有物流軌跡的
 * {
 *   "EBusinessID": "1109259",
 *   "OrderCode": "",
 *   "ShipperCode": "SF",
 *   "LogisticCode": "118461988807",
 *   "Success": false,
 *   "Reason": null
 *   "Traces": [],
 * }
 * 有物流軌跡的
 * {
 *   "EBusinessID": "1109259",
 *   "OrderCode": "",
 *   "ShipperCode": "SF",
 *   "LogisticCode": "118461988807",
 *   "Success": true,
 *   "State": 3,
 *   "Reason": null,
 *   "Traces": [{
 *     "AcceptTime": "2014/06/25 08:05:37",
 *     "AcceptStation": "正在派件..(派件人:鄧裕富,電話:18718866310)[深圳 市]",
 *     "Remark": null
 *   }]
 * }
 */
exports.track = {
  validate: {
    params: waybillSchemas.routeParams
  },
  handler: async (request, h) => {
    try {
      const waybillNo = request.params.waybill_no.replace(/\s+/g, '').trim()
      const shipperCode = request.params.shipper_code.replace(/\s+/g, '').trim()

      const requestData = {
        LogisticCode: waybillNo,
        ShipperCode: shipperCode
      }

      const responseData = await Kdniao.call('POST', RequestEnum.Track.path, {
        body: httpClient.encryptBody(requestData, RequestEnum.Track.type)
      }).then(result => {
        return result
      }).catch(err => {
        throw boom.badRequest(`系統錯誤: ${err.response_body}`)
      })

      const output = {
        success: responseData.Success,
        waybill_no: responseData.LogisticCode,
        shipper_code: responseData.ShipperCode,
        state: responseData.State || 0,
        reason: responseData.Success ? chinese.s2t(responseData.Reason) : '找不到資料，可能運送公司與單號不相符！',
        traces: responseData.Traces.map((trace, idx, ary) => {
          return {
            accept_time: trace.AcceptTime,
            accept_station: chinese.s2t(trace.AcceptStation),
            remark: chinese.s2t(trace.Remark)
          }
        })
      }

      return h.response(output).code(200)
    } catch (e) {
      return h.badRequest(`系統錯誤: ${e.message}`)
    }
  }
}
